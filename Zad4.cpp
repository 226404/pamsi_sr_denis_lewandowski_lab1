#include <iostream>
#include <string>

using namespace std;

bool jestPal(string testStr) {
    if((testStr.length()==1) || (testStr.length()==0)) return true;
    if(testStr[0] == testStr[testStr.length()-1]) {
        return jestPal(testStr.substr(1, testStr.length()-2));  //Rekurencyjne wywolanie funkcji z krotszym o poczatek i koniec stringiem
    } else return false;
}

int main()
{
    string slowo;
    cout << "Witaj w programie PAMSI_4!" << endl;

    while(slowo!="0") {
    cout << "Podaj slowo: ";
    cin >> slowo;
    if(jestPal(slowo)) cout << "Jest palindromem!" << endl;
    else cout << "Nie jest palindromem!" << endl;
    }

    return 0;
}
