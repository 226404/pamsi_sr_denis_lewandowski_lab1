#include <iostream>
#include <fstream>
#include <string>

using namespace std;

void zapisDoPliku(int tablica[], int rozmiar) {
        string nazwaPliku;
        fstream plik;
        cout << "Podaj nazwe pliku: ";
        cin >> nazwaPliku;
        plik.open(nazwaPliku, ios::out);  // Otwarcie pliku do zapisu
        for(int i=0; i<rozmiar; i++)      // Zapis do pliku
            plik << tablica[i] << "\t";
        cout << endl;
        plik.close();                     // Zamnkniecie pliku
}

void wczytajPlik(int tablica[], int rozmiar) {
        string nazwaPliku;
        fstream plik;
        cout << "Podaj nazwe pliku: ";
        cin >> nazwaPliku;
        plik.open(nazwaPliku, ios::in);  // Otwarcie pliku do wczytu
        if(plik.good()) {                // Warunek sprawdzajacy poprawnosc wczytywania
            for(int i=0; i<rozmiar; i++)
                plik >> tablica[i];
            cout << "Wczytano pomyslnie!" << endl;
        } else cout << "Nie udalo sie wczytac pliku " << nazwaPliku << endl; // Jesli sie nie udalo :(
        plik.close();
}

void zapisDoPlikuBinarnego(int tablica[], int rozmiar) {
        string nazwaPliku;
        cout << "Podaj nazwe pliku binarnego do zapisu: ";
        cin >> nazwaPliku;
        ofstream plikBinarny(nazwaPliku, ios::binary);  //Otwarcie pliku binarnego do zapisu
        if(plikBinarny.is_open())                       // Czy plik zostal poprawnie otwarty?
        for(int i=0; i<rozmiar; i++)                    // Zapis do pliku binarnego
            plikBinarny.write((char*) &tablica[i], sizeof &tablica);
        cout << "Zapisano plik binarny!" << endl;
        plikBinarny.close();                            //Zamykanie pliku
}

int main()
{
    cout << "Witaj w programie PAMSI_LAB_2!" << endl;
    int rozmiar, opcja, *tablica;
    fstream plik;
    string nazwaPliku;
    cout << "Podaj dlugosc tablicy jednowymiarowej: ";
    cin >> rozmiar;
    tablica = new int [rozmiar]; // Alokowanie pamieci dla tablicy o zadanym rozmiarze

do {
    cout << "1.Wczytaj liczby do tablicy"<<endl;
    cout << "2.Wyswietl tablice" << endl;
    cout << "3.Zapisz do pliku"<<endl;
    cout << "4.Wczytaj z pliku"<<endl;
    cout << "5.Zapisz do pliku binarnego"<<endl;
    cout << "6.Wczytaj z pliku binarnego"<<endl;
    cout << "0.Koniec"<<endl;
    cout << "Wybierz opcje: ";
    cin >> opcja;

    switch(opcja) {
    case 0:
        delete [] tablica;
        return 0;
        break;
    case 1:
        for(int i=0; i<rozmiar; i++)
            cin >> tablica[i];
        cout << "Wczytano pomyslnie!" << endl;
        break;
    case 2:
        for(int i=0; i<rozmiar; i++)
            cout << tablica[i] << "\t";
        cout << endl;
        break;
    case 3:
        zapisDoPliku(tablica, rozmiar);
        break;
    case 4:
        wczytajPlik(tablica, rozmiar);
        break;
    case 5:
        zapisDoPlikuBinarnego(tablica, rozmiar);
        break;
    case 6: {
        cout << "Podaj nazwe pliku binarnego do odczytu: ";
        cin >> nazwaPliku;
        ifstream plikBinarny2(nazwaPliku, ios::binary);  //Otwarcie pliku binarnego do odczytu
        char *bufor = new char [sizeof(int)];            // bufor na dane
        if(plikBinarny2.good()) {
        for(int i=0; i<rozmiar; i++)                     // Wczytywanie danych z pliku bin do bufora
            plikBinarny2.read(&bufor[i], sizeof &tablica);
        for(int i=0;i<rozmiar;i++)                       // Kopiowanie danych z bufora do tablicy
            tablica[i]=bufor[i];
        } else cerr << "Blad otwarcia pliku binernego!" << endl;
        break; }
    default: cerr << "Bledny wybor!" << endl; break;
    }
} while(opcja!=0);

    return 0;
}
