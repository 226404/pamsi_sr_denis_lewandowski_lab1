#include <iostream>

using namespace std;

int Silnia(int x) {
    if(x==0) return 1;
    else return x*Silnia(x-1);
}

int Potega(int x, int p) {
    if(p==0) return 1;
    else return x*Potega(x,p-1);
}

int main()
{
    int opcja, x, p;
    cout << "Witaj w programie PAMSI_3!" << endl;
    cout << "Co chces zrobi�?" << endl;
    cout << "1. Potega" << endl;
    cout << "2. Silnia" << endl;

    cin >> opcja;
     if(opcja==1){
        cout << "Podaj x: ";
        cin >> x;
        cout << "Podaj p: ";
        cin >> p;
        cout << "x^p = " << Potega(x, p) << endl;
     }
    if(opcja==2){
        cout << "Podaj x: ";
        cin >> x;
        cout << "x! = " << Silnia(x) << endl;
     }

    return 0;
}
