#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int** StworzTablice(int **tablica, int n, int m) {
    // Tablica na wskazniki
    tablica = new int * [n];
    // Stworzenie tablicy dwuwymiarowej
    for(int i=0; i<n; i++) {
        tablica[i] = new int [m];
    }
    return tablica;
}

void WyswietlTablice(int **tablica, int n, int m) {
    for(int i=0; i<n; i++) {
        for(int j=0; j<m; j++) {
            cout << tablica[i][j] << "\t";
        }
        cout << endl;
    }
}

void WypelnijTablice(int **tablica, int n, int m) {
    int x;
    cout << "Podaj maksymalna wartosc losowanych liczb: ";
    cin >> x;
    for(int i=0; i<n; i++) {
        for(int j=0; j<m; j++) {
            tablica[i][j] = rand()%(x+1);
        }
    }
}

int ZnajdzWartoscMaksymalna(int **tablica, int n, int m) {
    int max=tablica[0][0]; // Pierwszy element tablicy
    for(int i=0; i<n; i++) {
        for(int j=0; j<m; j++) {
            if(max<tablica[i][j])
                max=tablica[i][j];
        }
    }
    return max;
}

int main()
{
    int **tablica;
    int opcja; // Zmienna do sterowania menu
    int x;     // Zakres losowanych liczb
    int n, m;  // Wymiary tablicy dynamicznej

    srand(time(NULL));

    cout << "Witaj w programie PAMSI_LAB_1!" << endl;
    cout << "Podaj liczbe wierszy: ";
    cin >> n;
    cout << "Podaj liczbe kolumn: ";
    cin >> m;
    tablica = StworzTablice(tablica, n, m);

do {
    cout << "1.Wypelnij tablice losowymi liczbami"<<endl;
    cout << "2.Wy�wietl tablice"<<endl;
    cout << "3.Wyswietl najwieksza liczbe w tablicy"<<endl;
    cout << "0.Koniec"<<endl;
    cout << "Wybierz opcje: ";
    cin >> opcja;

    switch(opcja)
    {
        case 0:
            // Zwolnienie pamieci
            for(int i=0; i<n; i++) {
            delete [] tablica[i];
            }
            delete tablica;
            break;
        case 1:
            WypelnijTablice(tablica, n, m);
            break;
        case 2:
            WyswietlTablice(tablica, n, m);
            break;
        case 3:
            cout << "Maksymalna wartosc: " << ZnajdzWartoscMaksymalna(tablica, n, m) << endl;
            break;
    }
} while(opcja !=0);

    return 0;
}
